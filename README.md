# zXML

zeen3's quick xml parser.

does whitespace removal, comment removal.

usage:

```javascript
import zXML from 'z-xml'
zXML.parse(buffer, 0, preserveWhiteSpace, preserveComments)
// [zXML_El, zXML_El, ...]
```

```javascript
const zXML = require('z-xml')
zXML.parse(buffer)
// [zXML_El, ...]
```

really more of a proof-of-concept than anything.

can be used for simple system makers or something. idk.

really just me playing with buffers.

