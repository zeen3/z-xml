export declare class zXML {
    private buf;
    private pos;
    private whitespace;
    private comments;
    constructor(buf: Uint8Array | Uint8ClampedArray, pos: number, whitespace: boolean, comments: boolean);
    static utf8_decode(utf8: Uint8ClampedArray): string;
    static utf8_encode(str: string): Uint8ClampedArray;
    private static readonly __SCRIPT;
    private static readonly __STYLE;
    private bufIndexOf;
    static readonly COMMENT: unique symbol;
    static readonly TEXT: unique symbol;
    static readonly DOCTYPE: unique symbol;
    static readonly OPEN: number;
    static readonly CLOSE: number;
    static readonly DASH: number;
    static readonly SLASH: number;
    static readonly BANG: number;
    static readonly QUOT: number;
    static readonly APOS: number;
    static readonly NAME_SPACER: number[];
    static parse(buf: Uint8Array | Uint8ClampedArray, pos: number, whitespace?: boolean, comments?: boolean): zXML_El[];
    private posExist;
    private parseChildren;
    private parseText;
    private parseName;
    private parseString;
    private parseAttrs;
    private parseNode;
}
export declare class zXML_El {
    tag: string | symbol;
    attr: {
        key: string;
        value: string;
    }[];
    children: zXML_El[];
    content?: Uint8ClampedArray;
    static readonly QUOT: string;
    static readonly APOS: string;
    constructor(tag: string | symbol, attr: {
        key: string;
        value: string;
    }[], children: zXML_El[], content?: Uint8ClampedArray);
    toString(): string;
    reduceAttr(s: string, attr: {
        key: string;
        value: string;
    }): string;
}
export default zXML;
