"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class zXML {
    constructor(buf, pos = 0, whitespace, comments) {
        this.buf = buf;
        this.pos = pos;
        this.whitespace = whitespace;
        this.comments = comments;
    }
    static utf8_decode(utf8) {
        const codes = [];
        for (let i = 0; i < utf8.length; i++)
            if ((utf8[i] & 0x80) === 0x80) {
                switch (utf8[i] & 0xF0) {
                    case 0xF0:
                        if ((0xFC & utf8[i]) === 0xFC) {
                            codes.push((0x03 & utf8[i++]) << 30 |
                                (0x3F & utf8[i++]) << 24 |
                                (0x3F & utf8[i++]) << 18 |
                                (0x3F & utf8[i++]) << 12 |
                                (0x3F & utf8[i++]) << 6 |
                                (0x3F & utf8[i]));
                            continue;
                        }
                        if ((0xF8 & utf8[i]) === 0xF8) {
                            codes.push((0x07 & utf8[i++]) << 24 |
                                (0x3F & utf8[i++]) << 18 |
                                (0x3F & utf8[i++]) << 12 |
                                (0x3F & utf8[i++]) << 6 |
                                (0x3F & utf8[i]));
                            continue;
                        }
                        codes.push((0x0F & utf8[i++]) << 18 |
                            (0x3F & utf8[i++]) << 12 |
                            (0x3F & utf8[i++]) << 6 |
                            (0x3F & utf8[i]));
                        continue;
                    case 0xE0:
                        codes.push((0x0F & utf8[i++]) << 12 |
                            (0x3F & utf8[i++]) << 6 |
                            (0x3F & utf8[i]));
                        continue;
                    case 0xD0:
                    case 0xC0:
                        codes.push((0x1F & utf8[i++]) << 6 |
                            (0x3F & utf8[i]));
                        continue;
                    default:
                        codes.push(utf8[i]);
                        continue;
                }
            }
            else {
                codes.push(utf8[i]);
                continue;
            }
        return String.fromCodePoint(...codes);
    }
    static utf8_encode(str) {
        const bytes = [];
        for (const character of str) {
            const d = 0 | Number(character.codePointAt(0));
            if (d < 0x80) {
                bytes.push(d);
                continue;
            }
            if (d < 0x800) {
                bytes.push((d >>> 6) | 0xC0);
                bytes.push((d & 0xBF) | 0x80);
                continue;
            }
            if (d < 0x10000) {
                bytes.push((d >>> 12) | 0xE0);
                bytes.push((d >>> 6) & 0x3F | 0x80);
                bytes.push((d & 0x3F) | 0x80);
                continue;
            }
            if (d < 0x110000) {
                bytes.push((d >>> 18) | 0xF0);
                bytes.push((d >>> 12) & 0x3F | 0x80);
                bytes.push((d >>> 6) & 0x3F | 0x80);
                bytes.push((d & 0x3F) | 0x80);
                continue;
            }
            if (d < 0x4000000) {
                bytes.push((d >>> 24) | 0xF8);
                bytes.push((d >>> 18) & 0x3F | 0x80);
                bytes.push((d >>> 12) & 0x3F | 0x80);
                bytes.push((d >>> 6) & 0x3F | 0x80);
                bytes.push((d & 0x3F) | 0x80);
                continue;
            }
            if (d < 0x100000000) {
                bytes.push((d >>> 30) | 0xFC);
                bytes.push((d >>> 24) & 0x3F | 0x80);
                bytes.push((d >>> 18) & 0x3F | 0x80);
                bytes.push((d >>> 12) & 0x3F | 0x80);
                bytes.push((d >>> 6) & 0x3F | 0x80);
                bytes.push((d & 0x3F) | 0x80);
                continue;
            }
        }
        return Uint8ClampedArray.from(bytes);
    }
    bufIndexOf(search, idx = 0) {
        while (idx !== -1) {
            for (let i = 0; i < search.length; i++) {
                if (search[i] !== this.buf[idx + i])
                    break;
                if (i === search.length - 1)
                    return idx + i;
            }
            idx = this.buf.indexOf(search[0], idx + 1);
        }
        return idx;
    }
    static parse(buf, pos, whitespace = true, comments = true) {
        return new zXML(buf, pos, whitespace, comments).parseChildren();
    }
    posExist() {
        return this.buf[this.pos] !== undefined;
    }
    parseChildren() {
        const children = [];
        while (this.buf[this.pos])
            switch (this.buf[this.pos]) {
                case zXML.OPEN: switch (this.buf[this.pos + 1]) {
                    case zXML.SLASH:
                        console.log('element close');
                        this.pos = this.buf.indexOf(zXML.CLOSE, this.pos);
                        if (this.pos + 1)
                            this.pos += 1;
                        return children;
                    case zXML.BANG:
                        if (this.buf[this.pos + 2] === this.buf[this.pos + 3] && this.buf[this.pos + 2] === zXML.DASH) {
                            const start = this.pos + 4;
                            console.log('comment', this.pos);
                            this.pos = this.bufIndexOf([zXML.DASH, zXML.DASH, zXML.CLOSE], this.pos);
                            if (this.pos === -1)
                                this.pos = this.buf.length;
                            if (this.comments)
                                children.push(new zXML_El(zXML.COMMENT, [], [], new Uint8ClampedArray(this.buf.buffer, this.buf.byteOffset + start, this.pos === -1 ? (this.buf.length - start - 2) : this.pos - start - 2)));
                        }
                        else {
                            const start = this.pos + 2;
                            console.log('doctype');
                            this.pos = this.buf.indexOf(zXML.CLOSE, this.pos);
                            children.push(new zXML_El(zXML.DOCTYPE, [], [], new Uint8ClampedArray(this.buf.buffer, this.buf.byteOffset + start + 2, this.pos - start - 2)));
                        }
                        this.pos++;
                        continue;
                    default:
                        const node = this.parseNode();
                        children.push(node);
                        continue;
                }
                case 0x09:
                case 0x0A:
                case 0x0D:
                case 0x20:
                    if (!this.whitespace) {
                        this.pos++;
                        continue;
                    }
                    children.push(this.parseText());
                    this.pos++;
                    continue;
                default:
                    const text = this.parseText();
                    children.push(text);
                    this.pos++;
                    continue;
            }
        return children;
    }
    parseText() {
        let start = this.pos;
        this.pos = this.buf.indexOf(zXML.OPEN, this.pos) - 1;
        if (this.pos === -1)
            this.pos = this.buf.length;
        return new zXML_El(zXML.TEXT, [], [], new Uint8ClampedArray(this.buf.buffer, this.buf.byteOffset + start, this.pos - start));
    }
    parseName() {
        let start = this.pos;
        while (zXML.NAME_SPACER.indexOf(this.buf[this.pos]) === -1
            && this.posExist())
            this.pos++;
        console.log(start, this.pos);
        return zXML.utf8_decode(new Uint8ClampedArray(this.buf.buffer, this.buf.byteOffset + start, this.pos - start));
    }
    parseString() {
        let start = this.pos;
        let q = this.buf[this.pos++];
        this.pos = this.buf.indexOf(q, this.pos);
        console.log(start, q, this.pos);
        return zXML.utf8_decode(new Uint8ClampedArray(this.buf.buffer, this.buf.byteOffset + start + 1, this.pos - start - 1));
    }
    parseAttrs() {
        const attrs = [];
        while (this.buf[this.pos] !== zXML.CLOSE && this.posExist()) {
            const c = this.buf[this.pos];
            if ((c > 0x40 && c < 0x5A) || (c > 0x60 && c < 0x7A)) {
                let attr = this.parseName();
                let code = this.buf[this.pos];
                console.log(code, String.fromCodePoint(code), this.pos);
                while (this.posExist()
                    && code !== zXML.APOS
                    && code !== zXML.QUOT)
                    console.log(code = this.buf[++this.pos], String.fromCodePoint(code), this.pos);
                let value = code === zXML.APOS || code === zXML.QUOT
                    ? this.parseString()
                    : (this.pos--, attr);
                if (this.pos === -1)
                    return attrs;
                attrs.push({
                    key: attr,
                    value: value
                });
            }
            this.pos++;
        }
        return attrs;
    }
    parseNode() {
        this.pos++;
        const tag = this.parseName();
        const attrs = this.parseAttrs();
        const start = this.pos + 1;
        if (this.buf[this.pos - 1] !== zXML.SLASH)
            switch (tag) {
                case 'br':
                case 'img':
                case 'input':
                case 'link':
                case 'meta':
                case 'wbr':
                    return new zXML_El(tag, attrs, []);
                case 'script':
                    this.pos = this.bufIndexOf(zXML.__SCRIPT, this.pos) + 1;
                    return new zXML_El(tag, attrs, [], new Uint8ClampedArray(this.buf.buffer, this.buf.byteOffset + start, (this.pos - start) - zXML.__SCRIPT.length));
                case 'style':
                    this.pos = this.bufIndexOf(zXML.__STYLE, this.pos) + 1;
                    return new zXML_El(tag, attrs, [], new Uint8ClampedArray(this.buf.buffer, this.buf.byteOffset + start, (this.pos - start) - zXML.__STYLE.length));
                default:
                    this.pos++;
                    return new zXML_El(tag, attrs, this.parseChildren());
            }
        else {
            this.pos++;
            return new zXML_El(tag, attrs, []);
        }
    }
}
zXML.__SCRIPT = zXML.utf8_encode('</' + 'script>');
zXML.__STYLE = zXML.utf8_encode('</style>');
zXML.COMMENT = Symbol('comment');
zXML.TEXT = Symbol('text');
zXML.DOCTYPE = Symbol('doctype');
zXML.OPEN = 0x3C;
zXML.CLOSE = 0x3E;
zXML.DASH = 0x2D;
zXML.SLASH = 0x2F;
zXML.BANG = 0x21;
zXML.QUOT = 0x22;
zXML.APOS = 0x27;
zXML.NAME_SPACER = [
    0x09,
    0x0A,
    0x0D,
    0x20,
    0x2F,
    0x3D,
    0x3E
];
exports.zXML = zXML;
class zXML_El {
    constructor(tag, attr, children, content) {
        this.tag = tag;
        this.attr = attr;
        this.children = children;
        this.content = content;
    }
    toString() {
        switch (this.tag) {
            case zXML.TEXT: return zXML.utf8_decode(this.content);
            case zXML.DOCTYPE: return `<!${zXML.utf8_decode(this.content)}>`;
            case zXML.COMMENT: return `<!--${zXML.utf8_decode(this.content)}-->`;
            default: return `<${'string' === typeof this.tag ? this.tag : Symbol.keyFor(this.tag)}${this.attr.reduce(this.reduceAttr, '')}>${this.content ? zXML.utf8_decode(this.content) : this.children.join('')}</${'string' === typeof this.tag ? this.tag : Symbol.keyFor(this.tag)}>`;
        }
    }
    reduceAttr(s, attr) {
        return `${s} ${attr.key}="${attr.value.replace('"', zXML_El.QUOT).replace("'", zXML_El.APOS)}"`;
    }
}
zXML_El.QUOT = `&#${zXML.QUOT};`;
zXML_El.APOS = `&#${zXML.APOS};`;
exports.zXML_El = zXML_El;
exports.default = zXML;
//# sourceMappingURL=index.js.map