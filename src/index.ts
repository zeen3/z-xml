
export class zXML {
	// constructor
	constructor(private buf: Uint8Array | Uint8ClampedArray, private pos:number = 0, private whitespace:boolean, private comments:boolean) {}

	// for utf8 to string
	static utf8_decode(utf8: Uint8ClampedArray):string {
		const codes:number[] = []
		for (let i = 0; i < utf8.length; i++) if ((utf8[i] & 0x80) === 0x80) {
			switch (utf8[i] & 0xF0) {
				case 0xF0:
					if ((0xFC & utf8[i]) === 0xFC) {
						codes.push(
							(0x03 & utf8[i++]) << 30 |
							(0x3F & utf8[i++]) << 24 |
							(0x3F & utf8[i++]) << 18 |
							(0x3F & utf8[i++]) << 12 |
							(0x3F & utf8[i++]) << 6 |
							(0x3F & utf8[i])
						)
						continue
					}
					if ((0xF8 & utf8[i]) === 0xF8) {
						codes.push(
							(0x07 & utf8[i++]) << 24 |
							(0x3F & utf8[i++]) << 18 |
							(0x3F & utf8[i++]) << 12 |
							(0x3F & utf8[i++]) << 6 |
							(0x3F & utf8[i])
						)
						continue
					}
					codes.push(
						(0x0F & utf8[i++]) << 18 |
						(0x3F & utf8[i++]) << 12 |
						(0x3F & utf8[i++]) << 6 |
						(0x3F & utf8[i])
					)
					continue
				case 0xE0:
					codes.push(
						(0x0F & utf8[i++]) << 12 |
						(0x3F & utf8[i++]) << 6 |
						(0x3F & utf8[i])
					)
					continue
				case 0xD0:
				case 0xC0:
					codes.push(
						(0x1F & utf8[i++]) << 6 |
						(0x3F & utf8[i])
					)
					continue
				default:
					codes.push(utf8[i])
					continue
			}
		} else {
			codes.push(utf8[i])
			continue
		}
		return String.fromCodePoint(...codes)
	}
	// string to utf8 (less needed but useful)
	static utf8_encode(str:string):Uint8ClampedArray {
		const bytes:number[] = []
		for (const character of str) {
			const d:number = 0 | Number(character.codePointAt(0))
			if (d < 0x80) {
				// 1-byte
				bytes.push(d)
				continue
			}
			if (d < 0x800) {
				// 2-byte
				bytes.push((d >>> 6) | 0xC0)
				bytes.push((d & 0xBF) | 0x80)
				continue
			}
			if (d < 0x10000) {
				// 3-byte
				bytes.push((d >>> 12) | 0xE0)
				bytes.push((d >>> 6) & 0x3F | 0x80)
				bytes.push((d & 0x3F) | 0x80)
				continue
			}
			if (d < 0x110000) {
				// 4-byte
				bytes.push((d >>> 18) | 0xF0)
				bytes.push((d >>> 12) & 0x3F | 0x80)
				bytes.push((d >>> 6) & 0x3F | 0x80)
				bytes.push((d & 0x3F) | 0x80)
				continue
			}
			if (d < 0x4000000) {
				// 5-byte (2018-05-27: Not mapped)
				bytes.push((d >>> 24) | 0xF8)
				bytes.push((d >>> 18) & 0x3F | 0x80)
				bytes.push((d >>> 12) & 0x3F | 0x80)
				bytes.push((d >>> 6) & 0x3F | 0x80)
				bytes.push((d & 0x3F) | 0x80)
				continue
			}
			if (d < 0x100000000) {
				// 6-byte (2018-05-27: Not mapped)
				bytes.push((d >>> 30) | 0xFC)
				bytes.push((d >>> 24) & 0x3F | 0x80)
				bytes.push((d >>> 18) & 0x3F | 0x80)
				bytes.push((d >>> 12) & 0x3F | 0x80)
				bytes.push((d >>> 6) & 0x3F | 0x80)
				bytes.push((d & 0x3F) | 0x80)
				continue
			}
		}
		return Uint8ClampedArray.from(bytes)
	}

	// constants
	private static readonly __SCRIPT = zXML.utf8_encode('</' + 'script>')
	private static readonly __STYLE = zXML.utf8_encode('</style>')
	private bufIndexOf(search: Uint8ClampedArray | number[], idx: number = 0):number {
		while (idx !== -1) {
			for (let i = 0; i < search.length; i++) {
				if (search[i] !== this.buf[idx + i]) break
				if (i === search.length-1) return idx + i
			}
			idx = this.buf.indexOf(search[0], idx + 1)
		}
		return idx
	}
	public static readonly COMMENT = Symbol('comment');
	public static readonly TEXT = Symbol('text');
	public static readonly DOCTYPE = Symbol('doctype');

	public static readonly OPEN = 0x3C;
	public static readonly CLOSE = 0x3E;

	public static readonly DASH = 0x2D;
	public static readonly SLASH = 0x2F;
	public static readonly BANG = 0x21;
	public static readonly QUOT = 0x22;
	public static readonly APOS = 0x27;

	public static readonly NAME_SPACER:number[] = [
		0x09, // \t
		0x0A, // \n
		0x0D, // \r
		0x20, // [space]
		0x2F, // /
		0x3D, // =
		0x3E // >
	]

	// parser
	static parse(buf:Uint8Array | Uint8ClampedArray, pos:number, whitespace:boolean = true, comments:boolean = true):zXML_El[] {
		return new zXML(buf, pos, whitespace, comments).parseChildren()
	}
	// simpler
	private posExist():boolean {
		return this.buf[this.pos] !== undefined
	}
	/**
	 * parses a list of entries
	 */
	private parseChildren():zXML_El[] {
		const children = []
		while (this.buf[this.pos]) switch (this.buf[this.pos]) {
			case zXML.OPEN: switch (this.buf[this.pos+1]) {
				case zXML.SLASH:
					// closing
					console.log('element close')
					this.pos = this.buf.indexOf(zXML.CLOSE, this.pos)
					if (this.pos + 1) this.pos += 1
					return children
				case zXML.BANG:
					// comments & doctype
					if (this.buf[this.pos + 2] === this.buf[this.pos + 3] && this.buf[this.pos + 2] === zXML.DASH) {
						// comments
						const start = this.pos + 4
						console.log('comment', this.pos)
						this.pos = this.bufIndexOf([zXML.DASH, zXML.DASH, zXML.CLOSE], this.pos)
						if (this.pos === -1)
							this.pos = this.buf.length
						if (this.comments) children.push(new zXML_El(
							zXML.COMMENT,
							[],
							[],
							new Uint8ClampedArray(
								this.buf.buffer,
								this.buf.byteOffset + start,
								this.pos === -1 ? (this.buf.length - start - 2) : this.pos - start - 2
							)
						))
					} else {
						// doctype support
						const start = this.pos + 2
						console.log('doctype')
						this.pos = this.buf.indexOf(zXML.CLOSE, this.pos)
						children.push(new zXML_El(
							zXML.DOCTYPE,
							[],
							[],
							new Uint8ClampedArray(this.buf.buffer, this.buf.byteOffset + start + 2, this.pos - start - 2)
						))
					}
					this.pos++
					continue
				default:
					const node = this.parseNode()
					children.push(node)
					continue
			}
			case 0x09:
			case 0x0A:
			case 0x0D:
			case 0x20:
				if (!this.whitespace) {
					this.pos++
					continue
				}
				children.push(this.parseText())
				this.pos++
				continue
			default:
				const text = this.parseText()
				children.push(text)
				this.pos++
				continue
		}
		return children
	}
	/*
	 * parses text until stopped
	 */
	private parseText():zXML_El {
		let start = this.pos
		this.pos = this.buf.indexOf(zXML.OPEN, this.pos)-1
		if (this.pos === -1)
			this.pos = this.buf.length
		return new zXML_El(
			zXML.TEXT,
			[],
			[],
			new Uint8ClampedArray(this.buf.buffer, this.buf.byteOffset + start, this.pos - start)
		)
	}
	/*
	 * parses name or the like until ended
	 */
	private parseName():string {
		let start = this.pos
		while (
			zXML.NAME_SPACER.indexOf(this.buf[this.pos]) === -1
			&& this.posExist()
		) this.pos++
		console.log(start, this.pos)
		return zXML.utf8_decode(new Uint8ClampedArray(this.buf.buffer, this.buf.byteOffset + start, this.pos - start))
	}
	/*
	 * parses to string
	 */
	private parseString():string {
		let start = this.pos
		let q = this.buf[this.pos++]
		this.pos = this.buf.indexOf(q, this.pos)
		console.log(start, q, this.pos)
		return zXML.utf8_decode(new Uint8ClampedArray(this.buf.buffer, this.buf.byteOffset + start + 1, this.pos - start - 1))
	}
	/*
	 * parses attributes on elements
	 */
	private parseAttrs():{key: string; value: string}[] {
		const attrs = []
		while (this.buf[this.pos] !== zXML.CLOSE && this.posExist()) {
			const c = this.buf[this.pos]
			if ((c > 0x40 && c < 0x5A) || (c > 0x60 && c < 0x7A)) {
				// [A-Za-z]
				let attr = this.parseName()
				let code = this.buf[this.pos]
				console.log(code, String.fromCodePoint(code), this.pos)
				while (
					this.posExist()
					&& code !== zXML.APOS
					&& code !== zXML.QUOT
				) console.log(code = this.buf[++this.pos], String.fromCodePoint(code), this.pos)

				let value = code === zXML.APOS || code === zXML.QUOT
					? this.parseString()
					: (this.pos--, attr)

				if (this.pos === -1) return attrs

				attrs.push({
					key: attr,
					value: value
				})
			}
			this.pos++
		}
		return attrs

	}
	/*
	 * main text
	 */
	private parseNode():zXML_El {
		this.pos++
		const tag = this.parseName()
		const attrs = this.parseAttrs()
		const start = this.pos + 1
		if (this.buf[this.pos - 1] !== zXML.SLASH) switch (tag) {
			case 'br':
			case 'img':
			case 'input':
			case 'link':
			case 'meta':
			case 'wbr':
				return new zXML_El(
					tag,
					attrs,
					[]
				)
			case 'script':
				this.pos = this.bufIndexOf(zXML.__SCRIPT, this.pos) + 1
				return new zXML_El(
					tag,
					attrs,
					[],
					new Uint8ClampedArray(this.buf.buffer, this.buf.byteOffset + start, (this.pos - start) - zXML.__SCRIPT.length)
				)
			case 'style':
				this.pos = this.bufIndexOf(zXML.__STYLE, this.pos) + 1
				return new zXML_El(
					tag,
					attrs,
					[],
					new Uint8ClampedArray(this.buf.buffer, this.buf.byteOffset + start, (this.pos - start) - zXML.__STYLE.length)
				)
			default:
				this.pos++
				return new zXML_El(
					tag,
					attrs,
					this.parseChildren()
				)
		} else {
			this.pos++
			return new zXML_El(
				tag,
				attrs,
				[]
			)
		}
	}
}

export class zXML_El {
	public static readonly QUOT = `&#${zXML.QUOT};`
	public static readonly APOS = `&#${zXML.APOS};`
	constructor(
		public tag:string | symbol,
		public attr: {key: string; value: string;}[],
		public children: zXML_El[],
		public content?: Uint8ClampedArray
	) {}
	toString():string {
		switch (this.tag) {
			case zXML.TEXT: return zXML.utf8_decode(this.content)
			case zXML.DOCTYPE: return `<!${zXML.utf8_decode(this.content)}>`
			case zXML.COMMENT: return `<!--${zXML.utf8_decode(this.content)}-->`
			default: return `<${'string' === typeof this.tag ? this.tag : Symbol.keyFor(this.tag)}${this.attr.reduce(this.reduceAttr, '')}>${this.content ? zXML.utf8_decode(this.content) : this.children.join('')}</${'string' === typeof this.tag ? this.tag : Symbol.keyFor(this.tag)}>`
		}
	}
	reduceAttr(s:string, attr: {key: string; value: string;}):string {
		return `${s} ${attr.key}="${attr.value.replace('"', zXML_El.QUOT).replace("'", zXML_El.APOS)}"`
	}
}

export default zXML


